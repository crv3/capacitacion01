export interface Producto{
    _id?:     string;
    name:   string;
    categoria: Categoria;
    //Se cambio el id por _id
}

export enum Categoria {
    Bebidas = "Bebidas",
    Tecnologia = "Tecnologia",
    Ropa = "Ropa",
    Musica = "Musica",
}