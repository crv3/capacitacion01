import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { Producto } from '../../interface/productos.interface';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styles: [
  ]
})
export class ListarComponent implements OnInit {

  productos: Producto[]=[];

  constructor(private productosService: ProductosService) { }

  ngOnInit(): void {
    this.productosService.getproductos()
    .subscribe( productos => this.productos = productos);
  }
}
