import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [
  ]
})
export class AgregarComponent {

  miFormulario: FormGroup = this.fb.group({
    name:[,[Validators.required]],
    categoria:[],
  });

  constructor(private fb: FormBuilder,
              private productosService: ProductosService,
              private router: Router) { }

  guardar(){
    this.productosService.addNewProducto( this.miFormulario.value ).subscribe();
    this.router.navigateByUrl("productos/listado");
  }

}
