import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Producto } from '../interface/productos.interface';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  private readonly API = environment.api;
  constructor( private http: HttpClient ) { }

  addNewProducto(producto: Producto): Observable<Producto>{
    return this.http.post<Producto>(this.API, producto);
  }
  getproductos(): Observable<Producto[]>{
    return this.http.get<Producto[]>(this.API);
  }
}